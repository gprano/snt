
# Images Numériques

## 1) À quoi ressemble une image sur un ordinateur ?

On va écrire des fichiers d'image "à la main" dans l'ordinateur, dans un format très simple : Portable Pixmap.

### 1.1) Noir et Blanc

Dans une image en noir et blanc, il n'y a que deux couleurs possibles : on va représenter le noir par des 1 et le blanc par des 0.

Cliquez sur les cases pour dessiner l'image en noir et blanc de 8x8 pixels que vous voulez :

<style>
td{
  width: 20px;
  height: 20px;
  background-color: 'white';
  border: 1px solid black;
}

table{
  border-collapse: collapse;
  table-layout: fixed;
  box-sizing: initial;
}
</style>

<script type="text/javascript">
function toggle(el){
	if(el.style.backgroundColor!='black'){
  	el.style.backgroundColor='black';
  }
  else{
	el.style.backgroundColor = 'white';
  }
}
</script>

<table>
  <tr>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
  </tr>
    <tr>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
  </tr>
    <tr>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
  </tr>
    <tr>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
  </tr>
    <tr>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
  </tr>
    <tr>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
  </tr>
    <tr>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
  </tr>
    <tr>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
    <td onclick="toggle(this);"></td>
  </tr>
</table>


Puis ouvrez le logiciel Geany <img src="../geany.png" style="width:32px;"> et écrivez :

* sur la première ligne, pour donner le format du fichier : `P1`
* sur la deuxième ligne, la largeur et la hauteur de l'image : `8 8`
* sur les 8 lignes suivantes, les suites de 0 et de 1 qui correspondent à l'image que vous avez dessiné juste au dessus (il faut les séparer par des espaces).

Enregistrez ce fichier en le nommant `noirblanc.pnm`.

Ouvrez le fichier dans le logiciel de traitement d'image **Gimp**. Est-ce que c'est bien ce que vous avez dessiné ?

### 1.2) Niveaux de gris

Le format d'image est maintenant P2 et on aura le droit à des valeurs entre 0 (noir) et une valeur maximale qu'on indiquera sur la 3e ligne du fichier (qui correspond au blanc). Les valeurs intermédiaires donnent des gris plus ou moins sombres.

Créez dans Geany un nouveau fichier `gris.pnm` en recopiant dedans:
```
P2
9 4
9
4 4 4 4 9 6 6 6 6
4 5 5 4 9 6 5 5 6
4 5 5 4 9 6 5 5 6
4 4 4 4 9 6 6 6 6
```

Ouvrez le fichier dans Gimp:

* Lequel des deux petits carrés dans l'image a l'air plus clair, celui de droite ou celui de gauche ?
* Si vous regardez les valeurs que vous avez écrites dans le fichier, à quels nombres chaque carré correspond ? Lequel est donc le plus clair réellement ?

### 1.3) Couleurs

Pour représenter une image en couleur, le format sera **P3** et on aura trois valeurs par pixel, qui représentent la quantité de rouge, de vert et de bleu. C'est le format RVB (ou RGB en anglais) que vous connaissez peut-être déjà.

Créez dans Geany le fichier `couleur.pnm` suivant :
```
P3
3 6
255
228 3 3 228 3 3 228 3 3
255 140 0 255 140 0 255 140 0
255 237 0 255 237 0 255 237 0
0 128 38 0 128 38 0 128 38
0 77 255 0 77 255 0 77 255
117 7 135 117 7 135 117 7 135
```

* Que représente l'image ? (il aurait fallu plus que 3 pixels de large, mais ça aurait été long à écrire)
* Le code RVB du violet est (117,7,135). Quel est le code RVB du jaune ?

## 2) Traitement d'image avec python

Le problème d’écrire les images comme ça, c’est que c’est long ! C’est pour ça qu’on utilise en général un logiciel de dessin comme Gimp, mais on peut aussi automatiser des transformations avec un langage de programmation comme python.

Téléchargez l’image suivante et enregistrez là au même endroit que vos fichiers précédents : 
![https://nodfs.xyz/pomme.png](https://nodfs.xyz/pomme.png) 


Puis ouvrez le logiciel **Thonny** et écrivez ce code:

```python
from PIL import Image
image = Image.open("pomme.png")
nb_ligne, nb_colonne = image.size
for i in range(nb_ligne):
    for j in range(nb_colonne):
        rouge,vert,bleu = image.getpixel((i,j))
        image.putpixel((i,j),(rouge,bleu,vert))
image.show()
```

Enregistrez le avec le nom `script_image.py` au même endroit, et exécutez-le.

* Quelle transformation sur l'image fait ce programme ?
* En regardant attentivement le code, expliquez pourquoi.

Pour transformer une image en niveaux de gris, on peut faire la moyenne des valeurs RVB et mettre cette moyenne comme valeur pour chaque couleur.

Modifiez le programme en rajoutant après la 6e ligne une ligne qui crée une variable `moyenne`, égale à la moyenne de rouge,vert,bleu. Modifier la ligne suivante pour mettre `moyenne` comme valeur des couleurs rouge,vert,bleu.

Exécutez le programme, et vérifiez qu'il affiche bien l'image en niveaux de gris.

## 3) Pour mieux comprendre les formats d'image (PNM, PNG, JPG)

Ouvrez le fichier pomme.png avec Gimp, et exportez-le au format PNM ("pomme.pnm", choisissez ASCII au lieu de Raw) puis au format JPG ("pomme.jpg", choisissez 10\% pour la qualité de l'image).

* Quelles sont les tailles de ces trois fichiers ? (PNM,PNG,JPG)

* En les ouvrant avec la visionneuse d'image, est-ce qu'on voit des différences ?

!!! info "Info"
    * PNM est un format non compressé donc plus lourd.
    * PNG utilise de la compression "sans perte" donc c'est plus léger et l'image ne
      perd pas de qualité.
    * JPG utilise de la compression "avec perte", qui permet d'avoir un fichier encore
      plus petit mais on perd en qualité.
      
* On ne peut pas ouvrir le fichier JPG avec Geany, qui ne reconnaîtra pas l'encodage. On peut quand même essayer
d'en afficher le contenu en faisant un clic droit dans le dossier qui le contient et "Ouvrir un terminal", puis dans
le terminal taper la commande `cat pomme.jpg` (`cat` est une commande qui affiche le contenu d'un fichier).

Est-ce que ça ressemble à ce qu'on a écrit dans la première partie ?

!!! info "Info"
    Le format PNM est un format texte, qu’on peut lire dans un éditeur de texte. Les formats PNG et JPG sont des formats binaires, ce qui est un peu plus efficace pour la taille des fichiers mais est difficile à lire et écrire pour un humain (et il faut un éditeur binaire ou hexadécimal pour l'afficher, vous pouvez essayer avec le logiciel `ghex`).

## 4) Stéganographie

La **cryptographie** sert à rendre une information illisible pour qui n’a pas la clé. La **stéganographie** est l’art de cacher une information pour qu’on ne remarque même pas qu’elle est là.

Il y a une image cachée dans l’image de pomme !
L’image caché est en noir et blanc, et pour chaque pixel de l’image de pomme, on a juste un peu modifié la valeur du bleu pour qu’elle soit :

* paire si le pixel correspondant de l’image secrète est noir ;
* impaire si il est blanc ;

Reprenez le code précédent dans Thonny, et après la ligne 3 ajoutez pour créer une nouvelle image :

```python
secret = Image.new("RGB",image.size)
```

Puis dans la boucle après la ligne 6 on va tester si la valeur bleu est paire, ce qui veut dire que le pixel de l'image secrète est noir (donc 0,0,0 en RVB):

```python
if bleu % 2 == 0:
    secret.putpixel((i,j),(0,0,0))
```

Et sinon, c'est qu'il est blanc (255,255,255 en RVB):
```python
else:
    secret.putpixel((i,j),(255,255,255))
```

À vous de changer la dernière ligne pour afficher l'image `secret` au lieu de l'image `image`.

Que représente l'image secrète ?
