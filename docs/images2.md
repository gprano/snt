# TP sur les images : suite et approfondissements

### Compression par le format d'image (jpg,png) vs avec zip/7z

Reprenez la version `.pnm` de l'image de pomme créée avec Gimp dans le TP précédent, et :

* compressez la au format `.zip`
* compressez la au format `.7z`

Quelles tailles de fichier obtient-on par rapport à l'original ? Et par rapport à la version `.png`
de l'image ? Que peut-on conclure ?

Compressez le fichier `pomme.png` avec au format zip. Quelle taille obtient-on ? Pourquoi ?

### Couleurs

Les logiciels d'image permettent d'accentuer le contraste d'une image, c'est à dire rendre les
pixels clairs encore plus clairs, et les pixels sombres encore plus sombres.

Dessinez un graphique avec en abscisse la valeur originale d'un pixel (entre 0 et 255) et en
ordonnée la valeur modifiée (entre 0 et 255). Commencez par tracer la diagonale (qui représente
une transformation qui ne change pas le pixel), puis essayez de tracer la forme de la courbe qui
représente une accentuation du contraste.

## Manipulation d'image en python

### Niveaux de gris bis

En général, on ne fait pas exactement la moyenne des trois couleurs pour avoir l'image en niveaux de
gris mais plutôt `gris = int(0.299*rouge + 0.587*vert + 0.114*bleu)`.

Re-testez avec cette méthode et comparez le résultat avec la méthode précédente.

### Négatif de l'image

Reprendre le code de modification de l'image de pomme, et affichez le négatif de l'image : il suffit
de transformer chaque valeur `x` de couleur en la valeur symétrique `255-x`.

### Conversion en noir/blanc

Convertissez l'image non plus en niveaux de gris mais en noir et blanc. Pour cela on peut définir un
seuil, et mettre chaque pixel en blanc si la valeur de rouge+vert+bleu dépasse ce seuil et en noir
sinon. Expérimentez avec plusieurs valeurs de seuil.

### Détection de contours

Reprendre le code qui décode le secret dans l'image, et à la place faites de la détection de contours !

Pour cela, on ne va commencer à la 2e ligne en changeant `range(nb_ligne)` en `range(1,nb_ligne)`,
puis juste après avoir récupéré les valeurs rouge,vert,bleu du pixel (i,j), faites pareil
pour récupérer des valeurs rouge2,vert2,bleu2 du pixel (i-1,j) : celui juste au dessus.

Ensuite, mettez un pixel noir dans secret seulement si la valeur du rouge a suffisamment changé
avec le test `abs(rouge-rouge2) > 20`, et affichez le résultat. Vous pouvez varier la valeur de seuil
de 20, et utiliser les 3 couleurs pour plus de précision.

### Réduire la taille de l'image

Créez une image 4 fois plus petite en divisant le nombre de lignes et de colonnes par 2.
Puis choisissez la valeur de chaque pixel de l'image réduite en faisant la moyenne des 4 pixels
correspondant dans l'image d'origine.

### Réflexion

Comment pourrait-on cacher du texte à la place d'une image en noir et blanc comme on l'a fait au TP précédent ?

