# Internet : le résumé du cours

## Définition

Internet est un réseau de réseaux interconnectés.

Il est composé d'ordinateurs (PC, smartphones, serveurs, routeurs, ...) reliées par différents moyens (wifi, réseau mobile, câble, fibres optique, ...).
Les connections entre les continents sont faites par des câbles sous-marins.

## Adresses IP

À chaque machine sur internet est attribuée une adresse IP.

!!! info 
	C'est le réseau qui attribue l'adresse IP à l'ordinateur, donc un même ordinateur pourra avoir une adresse
	IP différente selon l'endroit où il se connecte.

Les adresses IP de la version 4 (IPv4) sont composées de 4 nombres entre 0 et 255.

Par exemple, **192.168.0.1** est une adresse IPv4.

!!! info
	On appelle _octet_ un nombre entre 0 et 255, parce qu'il peut être représenté par huit _bits_,
	les chiffres binaires qui valent 0 ou 1. Par exemple 01100001 correspond à 97.

Le problème avec IPv4 est qu'il n'y a plus d'adresses disponibles (256 puissance 4 possibilités : environ 4 milliards).

La version 6 du protocole utilise des adresses beaucoup plus longues, qu'on écrit généralement en
hexadécimal (avec des chiffres et des lettres entre a et f).

Par exemple, **2a01:e0a:250:7460:6b3f:c0c:70cc:2460** est une adresse IPv6.

## Routage 

Les machines intermédaires qui se chargent de renvoyer les paquets dans la bonne direction sur le réseau
sont appelés les routeurs.

Le routage fonctionne avec les adresses IP : chaque routeur a une **table de routage** qui lui donne les règles
de la machine suivante à laquelle il faut envoyer le paquet en fonction de l'adresse de destination.

!!! info
	Comme il manque des adresses IPv4, les routeurs des réseaux locaux font souvent du NAT, c'est à dire de la traduction
	d'adresses IP. Dans ce cas toutes les machines d'un réseau local partagent la même adresse IP sur internet. Les machines
	sont différenciées par une adresse IP locale, qui n'a pas besoin d'être unique sur tout l'Internet.

## Protocoles

Les protocoles sont les langages codifiés que parlent les machines entre elles pour fonctionner.

Par exemple :

* Le protocole **TCP/IP** se charge de la transmission des paquets vers leur destination.
* Le protocole **HTTP** ou **HTTPS** sert à accéder aux sites web.
* Le protocole **DNS** sert à faire le lien entre nom de domaine (par exemple wikipedia.fr) et adresse IP (celle de wikipedia est 51.254.200.228).
* Le protocole **IMAP** sert à lire les e-mails sur un serveur.

## Vie Privée

Tous les intermédiaires qui font transiter vos paquets internet, comme votre fournisseur d'accès,
peuvent essayer de regarder ce qu'il y a dedans.

1. Pour le **contenu** des paquets, la plupart du traffic internet est désormais **chiffré**. Cela
empêche tous les intermédiaire de lire le contenu, car il faut avoir la clé qui n'est connue que de
l'expéditeur et du destinataire. C'est le cas lorsque vous visitez des sites en HTTP**S**.

2. Par contre, les intermédiaires doivent connaître les **adresses IP d'origine et de destination** pour
pouvoir faire le routage. Ces adresses donnent des informations :
 
	 * localisation géographique approximative avec votre adresse.
	* on peut savoir le nom de domaine du site que vous visitez si on a son adresse IP.

Cela signifie que l'usage d'internet n'est pas véritablement anonyme. Si vous utilisez un site internet,
celui-ci connaît votre adresse IP, et votre fournisseur d'accès sait quel abonné utilise cette adresse IP.
Une enquête judiciaire peut aller leur demander ces informations pour retrouver quelqu'un.

!!! info
	Il existe des manières de cacher plus ou moins bien son adresse IP sur Internet, par exemple en utilisant
	des serveurs proxy ou VPN, ou en utilisant le réseau Tor. Ces techniques sont utilisées pour des activités
	illégales, mais aussi pour échapper à la surveillance dans certains pays, ou pour protéger les sources de
	journalistes.

